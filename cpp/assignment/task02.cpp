/** Author: Mingachev Eldar **/

/**
 * 1. Реализовать класс дерева Меркле.
 * 2. Написать тестовое приложение которое
 * (a) Содержит 2 потока исполнения которые делят один список строк.
 * (b) Первый поток исполнения генерирует случайную строку длиной 256 символов и добавляет ее в очередь.
 * (c) Второй поток ждет пока в списке будет 4 строки,
 * после того как есть 4 строки он строит дерево Меркле для этих строк используя хэш-функцию SHA-256.
 * После построения дерева корневой хэш записывается в лог файл, дерево удаляется, строки удаляются из очереди.
 * (d) После того как в лог записано 128 корневых хэшей программа завершается.
 **/


#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>
#include <list>
#include <string>

#include "picosha2.h"

using namespace std;

// Binary Merkle tree.
class merkle_tree {
private:
    string hash;
    vector<merkle_tree> child_nodes;

    // Hash generation function. Using SHA256 by PicoSHA2.
    static const string new_hash(const string &src) {
        return picosha2::hash256_hex_string(src);
    }

    // Recursive hash calculation function.
    string _calc_hash() {
        string result;
        if (child_nodes.size() == 2) {
            // If node has 2 children - call function on them.
            string front = child_nodes.front()._calc_hash();
            string back = child_nodes.back()._calc_hash();
            // Check if children are leaf nodes and save their hash to result if true.
            if (front.empty()) {
                result += child_nodes.front().hash;
            }
            if (back.empty()) {
                result += child_nodes.back().hash;
            }
            result += front + back;
            return new_hash(result);
        } else if (child_nodes.size() == 1) {
            // If node has 1 child - child is a leaf. Return child's hash.
            result = child_nodes.front().get_hash();
            return result;
        } else {
            // If node has no children - node is a leaf. Return empty string.
            result = "";
            return result;
        }
    }

public:

    merkle_tree() = default;

    explicit merkle_tree(string hash) : hash(std::move(hash)) {}

    const string &get_hash() const {
        return hash;
    }

    void set_hash() {
        this->hash = _calc_hash();
    }

    // Whenever user adds child nodes/leaves - recalculates parent's hash.
    void set_child_nodes(const vector<merkle_tree> &child_nodes) {
        merkle_tree::child_nodes = child_nodes;
        set_hash();
    }

    // Whenever user adds child node/leaf - recalculates parent's hash.
    bool add_child(const merkle_tree &child) {
        if (child_nodes.size() >= 2)
            return false;
        else
            child_nodes.push_back(child);
        this->set_hash();
        return true;
    }

    bool new_child(const string &data) {
        return add_child(merkle_tree(
                new_hash(data)
        ));
    }

};

// String length, list max capacity, hash entries amount.
const int S_SIZE = 256, L_SIZE = 4, H_SIZE = 128;
int queued = 0, added = 0, processed = 0;

mutex list_mutex;
list<string> src; // source string list
list<string> out; // output string list

// Write generated root hashes to the log file.
void log() {
    ofstream output("log.bin", ios::out | ios::binary | ios::app);
    char *hash;
    for (const string &s: out) {
        hash = (char *) s.c_str();
        output << hash << endl;
    }
    output.close();
}

// Generate random character.
char random_char() {
    random_device rd;
    uniform_int_distribution<char> uid(-128, 127);
    return uid(rd);
}

// Generate new string of S_SIZE (256) random characters.
string new_string() {
    string data;
    for (int i = 0; i < S_SIZE; ++i)
        data += random_char();
    return data;
}

// Add generated string to the list.
void add_string() {
    src.push_back(new_string());
    queued++;
    added++;
}

// Looping wrapper method for "add_string" method.
void string_handler() {
    while (added < H_SIZE) {
        if (queued == 0) {
            lock_guard<mutex> lock(list_mutex);
            for (int j = 0; j < L_SIZE; ++j) {
                cout << "1st thread: Added - " << added
                     << ". Queued - " << queued
                     << ". Processed - " << processed << endl;
                add_string();
            }
            // Wait until all queued strings are processed.
            this_thread::sleep_for(chrono::milliseconds(100));
        }
    }
}

// Generate new Merkle tree from leaves data.
merkle_tree new_tree(const string &left_leaf,
                     const string &right_leaf) {
    merkle_tree result = merkle_tree();
    result.new_child(left_leaf);
    result.new_child(right_leaf);
    return result;
}

// Generate new Merkle tree from prepared branches vector.
merkle_tree new_tree(const vector<merkle_tree> &branches) {
    merkle_tree result = merkle_tree();
    result.set_child_nodes(branches);
    return result;
}

// Add generated root (primary root and branches) hashes to the log file.
void add_tree() {
    vector<string> leaves{begin(src), end(src)};
    vector<merkle_tree> branches;
    merkle_tree child;
    for (int i = 0; i < L_SIZE; i += 2) {
        child = new_tree(leaves[i], leaves[i + 1]);
        branches.push_back(child);
    }
    merkle_tree result = new_tree(branches);
    out.push_back(result.get_hash());
    processed += 4;
    queued -= 4;
}

// Wrapper for "add_tree" method, list emptying and logging.
void tree_handler() {
    while (processed < H_SIZE) {
        lock_guard<mutex> lock(list_mutex);
        if (queued == 0) {
            // Wait until there are 4 strings in the list.
            this_thread::sleep_for(chrono::milliseconds(10));
            continue;
        }
        cout << "2nd thread: Added - " << added
             << ". Queued - " << queued
             << ". Processed - " << processed << endl;
        add_tree();
        src.clear();
    }
    log();
}

int main() {
    thread threads[2] = {
            thread(string_handler), thread(tree_handler)
    };
    for (auto &thread : threads) {
        thread.join();
    }
    return 0;
}