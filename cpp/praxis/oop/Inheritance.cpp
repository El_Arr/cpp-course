#include <iostream>

struct Foo {
private:
    const char *_msg;

protected:
    explicit Foo(const char *msg) : _msg(msg) {}

public:
    void say() const {
        std::cout << "Foo says: " << _msg << '\n';
    }
};

void foo_says(const Foo &foo) {
    foo.say();
}

struct Bar : Foo {
    explicit Bar(const char *msg) : Foo(msg) {}
};

Foo get_foo(const char *msg) {
    Bar b(msg);
    Foo f = b;
    return f;
}

int main() {
    foo_says(get_foo("Hello"));
    return 0;
}