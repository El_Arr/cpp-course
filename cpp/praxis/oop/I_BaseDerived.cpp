#include <iostream>

using namespace std;

struct Base {
    virtual void function1() {
        cout << "Base::f1()" << endl;
    }

    void function2() {
        cout << "Base::f2()" << endl;
    }
};


struct Derived : Base {
    virtual void function1() {
        cout << "Derived::f1()" << endl;
    }

    void function2() {
        cout << "Derived::f2()" << endl;
    }
};

int main() {
    Derived * pDer = new Derived();
    Base * pDer_copy = pDer;
    pDer -> Base::function1();
    pDer->function1();
    pDer->function2();
    pDer_copy->function1();
    pDer_copy->function2();
}