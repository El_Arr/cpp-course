#include <iostream>

using namespace std;

struct IBase {
    virtual void foo(int n = 1) const = 0;

    virtual ~IBase() = 0;
};

void IBase::foo(int n) const {
    cout << n << "foo\n";
}

IBase::~IBase() {
    cout << "Base destructor\n";
}

struct Derived : IBase {
    virtual void foo(int n = 2) const {
        IBase::foo(n);
    }

    void bar(const IBase &arg) {
        arg.foo();
    }
};

int main() {
    Derived().bar(Derived());
    return 0;
}