#include <iostream>

using namespace std;

struct Person {
protected:
    string _name;
    int _age{};

public:
    Person(string name, int age) : _name(name), _age(age) {}

    explicit Person(string name) : _name(name) {}

    int age() const {
        return _age;
    }

    virtual string name() const {
        return occupation() + _name + " ";
    }

    virtual string occupation() const = 0;
};

struct Student : Person {
private:
    string _uni;

public:
    Student(string name, int age, string uni) : Person(name, age), _uni(uni) {}

    string occupation() const {
        return "stud.";
    }

    string uni() const {
        return _uni;
    }
};

struct Professor : Person {
public:
    explicit Professor(string name) : Person(name) {}

    string occupation() const {
        return "Prof.";
    }
};

int main() {
    Professor pr("Stroustrup");
    cout << pr.name() << endl;
    Person *p = &pr;
    cout << p->name() << endl;
    return 0;
}