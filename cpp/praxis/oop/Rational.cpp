#include <cmath>

struct Rational {
private:
    int numerator_;
    int denominator_;

public:
    Rational() {
        numerator_ = 0;
        denominator_ = 1;
    }

    Rational(int num, int den = 1) :
            numerator_(num), denominator_(den) {
        reduce();
    }

    Rational reduce() {
        for (auto i = (int) fabs(numerator_); i > 0; --i) {
            if ((numerator_ % i == 0) && (denominator_ % i == 0)) {
                numerator_ /= i;
                denominator_ /= i;
            }
        }
        return *this;
    }

    Rational add(Rational const &r) const {
        if (this->denominator_ == r.denominator_) {
            return Rational(
                    this->numerator_ + r.numerator_,
                    this->denominator_
            );
        } else {
            return Rational(
                    this->numerator_ * r.denominator_ + r.numerator_ * this->denominator_,
                    this->denominator_ * r.denominator_
            );
        }
    }

    Rational sub(Rational const &rational) const {
        return this->add(rational.neg());
    }

    Rational mul(Rational const &rational) const {
        return Rational(
                this->numerator_ * rational.numerator_,
                this->denominator_ * rational.denominator_
        );
    }

    Rational div(Rational const &rational) const {
        return this->mul(rational.reverse());
    }

    Rational neg() const {
        return Rational(
                -(this->numerator_), (this->denominator_)
        );
    }

    Rational reverse() const {
        return Rational(
                this->denominator_, this->numerator_
        );
    }

    explicit operator double() {
        return (double) numerator_ / (double) denominator_;
    }

    int getNumerator_() const;

    int getDenominator_() const;

};

int Rational::getNumerator_() const {
    return numerator_;
}

int Rational::getDenominator_() const {
    return denominator_;
}

Rational operator-(Rational const &r) {
    return r.neg();
}

Rational operator+(Rational const &r1, Rational const &r2) {
    return r1.add(r2);
}

Rational operator+=(Rational const &r1, Rational const &r2) {
    return r1 + r2;
}

Rational operator-(Rational const &r1, Rational const &r2) {
    return r1.sub(r2);
}

Rational operator-=(Rational const &r1, Rational const &r2) {
    return r1 - r2;
}

Rational operator*=(Rational const &r1, Rational const &r2) {
    return r1.mul(r2);
}

Rational operator/=(Rational const &r1, Rational const &r2) {
    return r1 *= r2.reverse();
}

bool operator==(Rational const &r1, Rational const &r2) {
    return r1.getNumerator_() * r2.getDenominator_() == r2.getNumerator_() * r1.getDenominator_();
}

bool operator!=(Rational const &r1, Rational const &r2) {
    return r1.getNumerator_() * r2.getDenominator_() != r2.getNumerator_() * r1.getDenominator_();
}

bool operator>(Rational const &r1, Rational const &r2) {
    return r1.getNumerator_() * r2.getDenominator_() > r2.getNumerator_() * r1.getDenominator_();
}

bool operator>=(Rational const &r1, Rational const &r2) {
    return r1.getNumerator_() * r2.getDenominator_() >= r2.getNumerator_() * r1.getDenominator_();
}

bool operator<(Rational const &r1, Rational const &r2) {
    return r1.getNumerator_() * r2.getDenominator_() < r2.getNumerator_() * r1.getDenominator_();
}

bool operator<=(Rational const &r1, Rational const &r2) {
    return r1.getNumerator_() * r2.getDenominator_() <= r2.getNumerator_() * r1.getDenominator_();
}


int main() {
    Rational r1(5, 6);
    Rational rt = 5 + r1;
    Rational r2 = r1 + 5;
    return 0;
}