#include <iostream>

using namespace std;

struct Unit {

    explicit Unit(size_t id) : id_(id) {}

    size_t id() const {
        return id_;
    }

private:
    size_t id_;
};

struct Animal : virtual Unit {
    Animal(string const &name, size_t id) : Unit(id), name_(name) {}

    string const &name() const {
        return name_;
    }

private:
    string name_;
};

struct Man : virtual Unit {
    explicit Man(size_t id) : Unit(id) {}
};

struct Bear : Animal {
    explicit Bear(size_t id) : Unit(id), Animal("Bear", id) {};
};

struct Pig : Animal {
    explicit Pig(size_t id) : Unit(id), Animal("Pig", id) {};
};

struct ManBearPig : Man, Bear, Pig {
    ManBearPig(size_t id) : Unit(id), Man(id), Bear(id), Pig(id) {};
};

int main() {
    int a = 0;
    int d = (++a)++;
    cout << a << endl;
    cout << d << endl;
};