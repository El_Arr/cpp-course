struct Vector2D {
    double x, y;

    Vector2D(double x, double y) : x(x), y(y) {}

    Vector2D multiply(double d) const {
        return Vector2D(x * d, y * d);
    }

    double multiply(Vector2D const &p) const {
        return x * p.x + y * p.y;
    }
};

int main() {
    Vector2D t(1, 2);
    Vector2D m = t.multiply(4);
    double r = t.multiply(m);
}