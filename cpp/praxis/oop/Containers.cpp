#include <iostream>
#include <list>

template<class It>
size_t max_increasing_length(It p, It q) {
    if (p == q || *p > *q) {
        return 0;
    } else {
        It t = p;
        size_t c = 0;
        size_t cmax = 0;
        for (It i = p; i != q; i++) {
            It k = ++i;
            i--;
            if (*i < *k) {
                if (c == 0) {
                    t = i;
                }
                c++;
                if (c > cmax) {
                    cmax = c;
                }
            } else {
                c = 0;
            }
        }
        return cmax;
    }
}

int main() {
    std::list<int> l1 = {7, 8, 9, 4, 5, 6, 1, 2, 3, 4};
    std::cout << max_increasing_length(l1.begin(), l1.end());
}