#include <iostream>
#include <typeinfo>

using namespace std;

struct Expression;

struct Visitor {
    virtual void visitExpression(Expression const *bo) = 0;

    virtual ~Visitor() {

    }
};

struct Expression {
    virtual double evaluate() const = 0;

    virtual void visit(Visitor *visitor) const = 0;

    virtual ~Expression() {}

    bool check_equals(Expression const *left, Expression const *right) {
        return (bool) (*(int **) left = *(int **) right);
    }
};

struct Number : Expression {
public:
    explicit Number(double value) : value(value) {}

    void visit(Visitor *visitor) const {
        visitor->visitExpression(this);
    }

    double evaluate() const {
        return value;
    }

private:
    double value;
};

struct BinaryOperation : Expression {
public:
    BinaryOperation(
            Expression const *left, char op, Expression const *right
    ) :
            left(left), op(op), right(right) {}

    void visit(Visitor *visitor) const {
        visitor->visitExpression(this);
    }

    double evaluate() const {
        switch (op) {
            case '+':
                return left->evaluate() + right->evaluate();
            case '-':
                return left->evaluate() - right->evaluate();
            case '*':
                return left->evaluate() * right->evaluate();
            case '/':
                return left->evaluate() / right->evaluate();
            default:
                return -1;
        }
    }

    Expression const *get_left() const {
        return left;
    }

    Expression const *get_right() const {
        return right;
    }

    char get_op() const {
        return op;
    }

    ~BinaryOperation() {
        delete left;
        delete right;
    }

private:
    Expression const *left;
    Expression const *right;
    char op;
};

struct PrintVisitor : Visitor {
public:
    void visitExpression(Expression const *e) {
        if (typeid(e) == Number) {
            cout << e->evaluate();
        } else if (typeid(e) == BinaryOperation) {
            cout << "( ";
            ((BinaryOperation *) e)->get_left()->visit(this);
            cout << " " << ((BinaryOperation *) e)->get_op() << " ";
            ((BinaryOperation *) e)->get_right()->visit(this);
            cout << " )";
        }
    }
};

int main() {
    Expression *n1 = new Number(4.5);
    Expression *n2 = new Number(5);
    Expression *n3 = new Number(3);
    Expression *sube = new BinaryOperation(n1, '*', n2);
    Expression *expr = new BinaryOperation(n3, '+', sube);
    PrintVisitor pv;
    pv.visitExpression(expr);
    delete expr;
    return 0;
}
