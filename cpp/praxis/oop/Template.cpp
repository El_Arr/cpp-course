#include <iostream>

using namespace std;

template<typename T>
class Array {
private:
    T *data_;
    size_t size_;

public:
    explicit Array(size_t size = 0, const T &value = T()) : size_(size), data_(value) {}

    Array(const Array &array) {
        this->size_ = array.size_;
        this->data_ = array.data_;
    }

    ~Array() {
        delete[]data_;
    }

    Array &operator=(const Array &array) {

    }

    size_t size() const {
        return size_;
    }
};

template<typename Num>
Num square(Num n) {
    return n * n;
}

template<typename Type>
void sort(Type *n, Type *p) {
}

void foo() {
    int a = square<int>(3);
    int b = square(a) + square(4);
    float *m = new float[10];
    sort(m, m + 10);
    //sort(m, &a);
}

template<typename U, typename T>
void copy_n(U *u, T *t, int n) {
    for (int i = 0; i < n; ++i) {
        t[i] = (T) u[i];
    }
};

template<typename T, size_t n>
size_t array_size(T(&a)[n]) {
    return n; // |    return n sizeof(a) / sizeof(T);
}

int main() {
    int ints[] = {1, 2, 3, 4};
    //double doubles[4] = {};
    //copy_n(ints, doubles, 4);
    //for (double i : doubles) {
    //    cout << i << endl;
    //}
    int *iptr = ints;
    double doubles[] = {3.14};
    cout << array_size(ints) << endl;
    return 0;
}