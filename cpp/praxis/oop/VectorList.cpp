#include <iostream>
#include <list>
#include <vector>

using namespace std;

template<typename T>
class VectorList {
public:
    VectorList() {};

    VectorList(size_t n, size_t num, T const &temp) {
        for (size_t i = 0; i < n; ++i) {
            vector<T> v;
            for (size_t j = 0; j < num; ++j) {
                v.push_back(temp);
            }
            data.push_back(v);
        }
    }

    template<typename U>
    VectorList(VectorList<U> const &a) {
        this->clear();
        for (vector<U> v: a.getData()) {
            vector<T> vector;
            for (U u: v) {
                vector.push_back(static_cast<T>(u));
            }
            data.push_back(vector);
        }
    }

    template<typename U>
    VectorList &operator=(VectorList<U> const &a) {

        return *this;
    }

    void clear() {
        if (data.empty()) {
            return;
        } else {
            for (vector<T> v: data) {
                for (T t: v) {
                    delete &t;
                }
                delete &v;
            }
            delete &data;
        }
    }

    size_t size() const {
        size_t c = 0;
        if (data.size() != 0) {
            for (vector<T> v: data) {
                for (T t: v) {
                    c++;
                }
            }
        }
        return c;
    }

    void push_back(T t) {
        *data - data.size() -1;
        if (v.size() < num) {
            v.push_back(t);
        } else {
            vector<T> k1;
            v = k1;
            v.push_back(t);
            data.push_back(v);
        }
    }

    T &pop_back() {
        vector<T> v = data.end();
        T t = v.end() - 1;
        v.pop_back();
        if (v.size() == 0) {
            data.pop_back();
        }
        return t;
    }

    void delete_unit(size_t i) {
        size_t c = 0;
        for (vector<T> v : data) {
            if (i == c++) {
                data.remove(v);
            }
        }
    }

    void print() {
        size_t c = 0;
        for (vector<T> v: data) {
            cout << "Vector " << c << ": ";
            for (T t: v) {
                cout << t << " ";
            }
            cout << endl;
            c++;
        }
    }

    const list<vector<T>> &getData() const {
        return data;
    }

private:
    list<vector<T>> data;
    static const size_t num = 5;
};

int main() {
    int n(32);
    VectorList<int> v(3, 4, n);
    v.print();
    cout << endl;
    VectorList<double> t1(v);
    v.print();
    cout << endl;
    VectorList<double> t2 = v;
    v.print();
    cout << endl;
    cout << t2.size();
    cout << endl;
    t2.push_back(3.5);
    t2.print();
}