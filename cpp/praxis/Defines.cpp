#define FOR_EACH(I, list) for (std::list<int>::const_iterator I = (list).begin (); I != (list).end (); ++I)

#define TO_STRING(something) std::string(#something)