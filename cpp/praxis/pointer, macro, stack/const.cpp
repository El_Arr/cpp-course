#include <iostream>

int main() {
    int const i = 1;
    int *p = 0;

    int const **pp = &p;

    *pp = &i;
    *p = 2;
    return 0;
}