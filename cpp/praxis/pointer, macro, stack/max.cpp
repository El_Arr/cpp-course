#include <iostream>

using namespace std;

bool max_elem(int *p, int *q, int *res) {
    if (p == q) return false;
    *res = *p;
    for (; p != q; ++p)
        if (*p > *res)
            *res = *p;
    return true;
}

int main() {
    int m[10] = {1, 8, 7, 10, 5, 6, 4, 3, 2, 9};
    int max = 0;
    if (max_elem(m, m + 10, &max))
        cout << "Maximum = " << max << endl;
    return 0;
}