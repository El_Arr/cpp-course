/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>

using namespace std;

int main() {
    #pragma omp parallel num_threads(8)
    {
        printf("Thread %d / %d: Hello, world!\n", omp_get_thread_num(), omp_get_num_threads());
    }

    return 0;
}

