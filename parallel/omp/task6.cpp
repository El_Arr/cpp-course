/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>

using namespace std;

int main() {
    const int N = 10;
    int a[N] = {12, 13, 14, 15, 16, 17, 18, 19, 20, 11},
            b[N] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 1},
            i, sumA = 0, sumB = 0;

    #pragma omp parallel for private(i) shared(sumA, sumB)
    for (i = 0; i < N; ++i) {
        sumA += a[i];
        sumB += b[i];

        printf("Thread %d / %d: sumA = %d, sumB = %d.\n", omp_get_thread_num(), omp_get_num_threads(), sumA, sumB);
    }
    printf("Parallel for: A.avg = %f, B.avg = %f.\n", sumA / (N * 1.0), sumB / (N * 1.0));

    sumA = 0;
    sumB = 0;

    #pragma omp parallel for reduction(+:sumA, sumB) private(i)
    for (i = 0; i < N; ++i) {
        sumA += a[i];
        sumB += b[i];

        printf("Thread %d / %d: sumA = %d, sumB = %d.\n", omp_get_thread_num(), omp_get_num_threads(), sumA, sumB);
    }
    printf("Parallel reduction: A.avg = %f, B.avg = %f.\n", sumA / (N * 1.0), sumB / (N * 1.0));

    return 0;
}

