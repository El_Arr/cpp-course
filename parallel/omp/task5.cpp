/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>

#include "rand.h"

using namespace std;

double avg(int **array, const size_t L, const size_t C) {
    int sum = 0;
    for (size_t i = 0; i < L; ++i) {
        for (size_t j = 0; j < C; ++j) {
            sum += *(*(array + i) + j);
        }
    }
    return sum / (L * C * 1.0);
}

int min(int **array, const size_t L, const size_t C) {
    int min = **array;
    int element;
    for (size_t i = 0; i < L; ++i) {
        for (size_t j = 0; j < C; ++j) {
            element = *(*(array + i) + j);
            if (element < min) {
                min = element;
            }
        }
    }
    return min;
}

int max(int **array, const size_t L, const size_t C) {
    int max = **array;
    int element;
    for (size_t i = 0; i < L; ++i) {
        for (size_t j = 0; j < C; ++j) {
            element = *(*(array + i) + j);
            if (element > max) {
                max = element;
            }
        }
    }
    return max;
}

int div3(int **array, const size_t L, const size_t C) {
    int counter = 0;
    for (size_t i = 0; i < L; ++i) {
        for (size_t j = 0; j < C; ++j) {
            if (*(*(array + i) + j) % 3 == 0) {
                counter++;
            }
        }
    }
    return counter;
}

int main() {
    const int L = 6;
    const int C = 8;
    int **d = new int *[L];

    for (int i = 0; i < L; ++i) {
        *(d + i) = new int[C];
        for (int j = 0; j < C; ++j) {
            *(*(d + i) + j) = random_int();
        }
    }

    #pragma omp parallel num_threads(4)
    #pragma omp sections
    {
        #pragma omp section
        {
            printf("Thread %d / %d: avg = %f.\n", omp_get_thread_num(), omp_get_num_threads(), avg(d, L, C));
        }
        #pragma omp section
        {
            printf("Thread %d / %d: min = %d.\n", omp_get_thread_num(), omp_get_num_threads(), min(d, L, C));
            printf("Thread %d / %d: max = %d.\n", omp_get_thread_num(), omp_get_num_threads(), max(d, L, C));
        }
        #pragma omp section
        {
            printf("Thread %d / %d: div3 = %d.\n", omp_get_thread_num(), omp_get_num_threads(), div3(d, L, C));
        }
    };

    return 0;
}