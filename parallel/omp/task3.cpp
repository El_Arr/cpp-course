/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>

using namespace std;

int main() {
    int a = 3;
    int b = 22;

    printf("Before zone 1: a = %d, b = %d.\n", a, b);
    #pragma omp parallel num_threads(2) private(a) firstprivate(b)
    {
        a += omp_get_thread_num();
        b += omp_get_thread_num();
        printf("Zone 1, thread %d / %d: a = %d, b = %d.\n", omp_get_thread_num(), omp_get_num_threads(), a, b);
    }
    printf("After zone 1: a = %d, b = %d.\n\n", a, b);

    a = 3;
    b = 22;

    printf("Before zone 2: a = %d, b = %d.\n", a, b);
    #pragma omp parallel num_threads(4) shared(a) private(b)
    {
        a -= omp_get_thread_num();
        b -= omp_get_thread_num();
        printf("Zone 2, thread %d / %d: a = %d, b = %d.\n", omp_get_thread_num(), omp_get_num_threads(), a, b);
    }
    printf("After zone 2: a = %d, b = %d.\n", a, b);

    return 0;
}

