/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>

using namespace std;

int main() {
    const int N = 210;
    int i, result = 0;

    #pragma omp parallel for shared(result) schedule(dynamic)
    for (i = 1; i <= N+N; i+=2) {
        #pragma omp atomic
        result += i;
    }

    printf("Result is %d.\n", result);

    return 0;
}