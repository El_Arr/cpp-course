/*
 * Author: Eldar Mingachev
 */

#include <random>

#ifndef OMP_RAND_H
#define OMP_RAND_H

// Generate random integer.
int random_int() {
    std::random_device rd;
    std::uniform_int_distribution<int> uid(-256, 255);
    return uid(rd);
}

#endif //OMP_RAND_H
