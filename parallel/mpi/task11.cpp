/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>

int main(int argc, char **argv) {
    const int N = 8;
    int size, rank, i, l, *x, *y,
            *part_x, *part_y,
            sum_pt = 0, sum = 0;

    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    x = new int[N];
    y = new int[N];
    if (rank == 0) {
        for (i = 0; i < N; ++i) {
            x[i] = i;
            y[i] = i;
        }
    }

    l = N / size;
    if (l > 0) {
        part_x = new int[l];
        part_y = new int[l];
        MPI_Scatter(x, l, MPI_INT, part_x, l, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatter(x, l, MPI_INT, part_y, l, MPI_INT, 0, MPI_COMM_WORLD);

        for (i = 0; i < l; ++i) sum_pt += part_x[i] * part_y[i];
        MPI_Reduce(&sum_pt, &sum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

        if (rank == 0) printf("Result: %d\n", sum);
    }

    MPI_Finalize();
}

