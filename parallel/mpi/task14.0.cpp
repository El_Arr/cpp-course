/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>

#include "print.h"

int main(int argc, char **argv) {
    const int M = 4, N = 3;
    int size, rank, i, j, l, sum,
            *a, *x, *z, *part_a, *part_z;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (M % size == 0) {
        a = new int[M * N];
        x = new int[N];
        if (rank == 0) {
            for (i = 0; i < M; ++i) {
                for (j = 0; j < N; ++j) {
                    a[i * N + j] = i * N + j;
                }
            }
            for (i = 0; i < N; ++i) {
                x[i] = i;
            }
        }
        l = N * (M / size);
        if (l > 0) {
            part_a = new int[l];
            MPI_Scatter(a, l, MPI_INT, part_a, l, MPI_INT, 0, MPI_COMM_WORLD);
            MPI_Bcast(x, N, MPI_INT, 0, MPI_COMM_WORLD);

            part_z = new int[M / size];
            for (j = 0; j < M / size; ++j) {
                sum = 0;
                for (i = 0; i < N; ++i) {
                    sum += part_a[j * M / size + i] * x[i];
                }
                part_z[j] = sum;
            }
            z = new int[M];
            MPI_Gather(part_z, M / size, MPI_INT, z, M / size, MPI_INT, 0, MPI_COMM_WORLD);

            MPI_Barrier(MPI_COMM_WORLD);
            if (rank == 0) {
                print_block(z, M, rank);
            }
        }
    }
    MPI_Finalize();
}


