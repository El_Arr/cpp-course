/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>

#include "print.h"

int main(int argc, char **argv) {
    const int M = 4, N = 4;
    int size, rank, i, j, l, *a, *b, *c,
            *part_a, *part_b, *part_c;

    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    a = new int[M * N];
    b = new int[M * N];
    c = new int[M * N];
    if (rank == 0) {
        for (i = 0; i < M; ++i) {
            for (j = 0; j < N; ++j) {
                a[i * M + j] = i * M + j;
                b[i * M + j] = i * M + j;
            }
        }
    }

    l = M * N / size;
    if (l > 0) {
        part_a = new int[l];
        part_b = new int[l];
        MPI_Scatter(a, l, MPI_INT, part_a, l, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatter(b, l, MPI_INT, part_b, l, MPI_INT, 0, MPI_COMM_WORLD);

        part_c = new int[l];
        for (i = 0; i < l; ++i) {
            part_c[i] = part_a[i] * part_b[i];
        }

        MPI_Gather(part_c, l, MPI_INT, c, l, MPI_INT, 0, MPI_COMM_WORLD);
        if (rank == 0) print_matrix(c, M, N, rank);
    }

    MPI_Finalize();
}

