cmake_minimum_required(VERSION 3.12)
project(mpi_)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_C_COMPILER mpicc)
set(CMAKE_CXX_COMPILER mpicxx)

find_package(MPI REQUIRED)

set(CMAKE_CXX_COMPILE_FLAGS ${CMAKE_CXX_COMPILE_FLAGS} ${MPI_COMPILE_FLAGS})
set(CMAKE_CXX_LINK_FLAGS ${CMAKE_CXX_LINK_FLAGS} ${MPI_LINK_FLAGS})

add_executable(
        mpi_
        src/task1.cpp
        src/task2.cpp
        src/task3.cpp
        src/task4.cpp
        src/task5.cpp
        src/task6.cpp
        src/task7.cpp
        src/task8.cpp
        src/task9.cpp
        src/task10.cpp
        src/task11.cpp
        src/task12.cpp
        src/task13.cpp
        src/task14.0.cpp
        src/task14.1.cpp
        src/task15.cpp
        src/task16.cpp
        src/rand.h
        src/print.h
)

include_directories(${MPI_INCLUDE_PATH})
target_link_libraries(mpi_ ${MPI_LIBRARIES})

