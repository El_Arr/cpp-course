/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>

using namespace std;

int main(int argc, char **argv) {
    const int N = 10;
    int *x, *part, i, l, size, rank,
            sum_pt = 0, sum = 0;

    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    x = new int[N];
    if (rank == 0)
        for (i = 0; i < N; ++i)
            x[i] = i;

    l = N / size;
    if (l > 0) {
        part = new int[l];
        MPI_Scatter(x, l, MPI_INT, part, l, MPI_INT, 0, MPI_COMM_WORLD);

        for (i = 0; i < l; ++i) sum_pt += abs(part[i]);
        MPI_Reduce(&sum_pt, &sum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

        if (rank == 0) printf("Result: %d\n", sum);
    }

    MPI_Finalize();
}

