/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>
#include "print.h"

int main(int argc, char **argv) {
    const int M = 2, N = 8;
    int size, rank, i, j, k,
            l = M * N, *a, *b, d[M * N];
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size == 4) {
        MPI_Datatype t;
        const int lengths[] = {N, N};
        const MPI_Aint displ[] = {0, 2 * N * N};
        const MPI_Datatype types[] = {MPI_INT, MPI_INT};
        MPI_Type_create_struct(2, lengths, displ, types, &t);
        MPI_Type_commit(&t);

        if (rank == 0) {
            a = new int[N * N];
            for (i = 0; i < N; ++i) {
                for (j = 0; j < N; ++j) {
                    a[i * N + j] = i * N + j;
                }
            }
            MPI_Send(&a[N], 1, t, 1, 1, MPI_COMM_WORLD);
            MPI_Send(&a[2 * N], 1, t, 2, 1, MPI_COMM_WORLD);
            MPI_Send(&a[3 * N], 1, t, 3, 1, MPI_COMM_WORLD);
            for (i = 0; i < N; ++i) {
                d[i] = a[i];
                d[N + i] = a[4 * N + i];
            }
            print_matrix(d, M, N, rank);
        }
        if (rank != 0) {
            MPI_Recv(d, M * N, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            print_matrix(d, M, N, rank);
        }
        MPI_Type_free(&t);
    }
    MPI_Finalize();
}

