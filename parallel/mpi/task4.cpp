/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>

#include "rand.h"
#include "print.h"

using namespace std;

int main(int argc, char **argv) {
    const int R = random_int() % 16 + 1, // [-256; 255]
            N = (R < 0) ? -R : R; // Avoid negative N
    int *a, i, size, rank;

    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 1) {
        a = new int[N];
        for (i = 0; i < N; ++i) a[i] = random_int();

        for (i = 0; i < size; ++i) {
            if (i == 1) continue;
            MPI_Ssend(a, N, MPI_INT, i, 0, MPI_COMM_WORLD);
        }
    } else {
        MPI_Probe(1, 0, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_INT, &i);
        a = new int[i];

        MPI_Recv(a, i, MPI_INT, 1, 0, MPI_COMM_WORLD, &status);
        print_block(a, i, rank);
    }
    delete a;

    MPI_Finalize();
}


