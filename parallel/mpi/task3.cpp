/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>

#include "rand.h"
#include "print.h"

using namespace std;

int main(int argc, char **argv) {
    const size_t N = 10;
    int size, rank, a[N];
    size_t i;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (rank == 0) {
        for (i = 0; i < N; ++i) a[i] = random_int();
        MPI_Send(a, N, MPI_INT, 1, 0, MPI_COMM_WORLD);
    } else if (rank == 1) {
        MPI_Recv(a, N, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
        print_block(a, N, rank);
    }
    MPI_Finalize();
}

