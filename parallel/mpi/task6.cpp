/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>

#include "print.h"

int main(int argc, char **argv) {
    const int A = 2, B = 5, N = 5;
    int size, rank, *x, *y, i, left, block;

    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0) {
        x = new int[N];
        y = new int[N];
        for (i = 0; i < N; ++i) {
            x[i] = i;
            y[i] = i;
        }

        block = N / (size - 1) + 1;
        left = N;
        for (i = 1; i < size; ++i) {
            print_block((left > 0) ? x + (i - 1) * block : x, (left > 0) ? ((left > block) ? block : left) : 0, rank);
            MPI_Send(
                    (left > 0) ? x + (i - 1) * block : y,
                    (left > 0) ? ((left > block) ? block : left) : 0,
                    MPI_INT, i, 0, MPI_COMM_WORLD
            );
            MPI_Send(
                    (left > 0) ? y + (i - 1) * block : y,
                    (left > 0) ? ((left > block) ? block : left) : 0,
                    MPI_INT, i, 1, MPI_COMM_WORLD
            );
            left -= block;
        }

        left = N;
        for (i = 1; i < size; ++i) {
            MPI_Probe(i, 2, MPI_COMM_WORLD, &status);
            MPI_Get_count(&status, MPI_INT, &left);
            MPI_Recv(x + (i - 1) * block, left, MPI_INT, i, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        print_block(x, N, rank);
    } else {
        MPI_Probe(0, 0, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_INT, &block);
        x = new int[block];
        y = new int[block];
        MPI_Recv(x, block, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(y, block, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        for (i = 0; i < block; ++i) {
            x[i] = A * x[i] + B * y[i];
        }
        MPI_Send(x, block, MPI_INT, 0, 2, MPI_COMM_WORLD);
    }
    MPI_Finalize();
}



