/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>

#include "print.h"

void transpose(const int *src, int *dst, const int N) {
    for (size_t i = 0; i < N * N; ++i) {
        dst[i] = src[N * (i % N) + i / N];
    }
}

int main(int argc, char **argv) {
    const int N = 5;
    int *a, *b, *c, *t, *d, i, j, k, z,
            size, rank, block, left, left_n;

    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size > 1) {
        if (rank == 0) {
            a = new int[N * N];
            b = new int[N * N];
            c = new int[N * N];
            for (i = 0; i < N; ++i) {
                for (j = 0; j < N; ++j) {
                    a[i * N + j] = i * N + j;
                    b[i * N + j] = i * N + j;
                }
            }
            // Simple multiplication
            block = N * N / (size - 1) + 1;
            left = N * N;
            for (i = 1; i < size; ++i) {
                MPI_Send(
                        (left > 0) ? a + (i - 1) * block : a,
                        (left > 0) ? ((left > block) ? block : left) : 0,
                        MPI_INT, i, 0, MPI_COMM_WORLD
                );
                MPI_Send(
                        (left > 0) ? b + (i - 1) * block : b,
                        (left > 0) ? ((left > block) ? block : left) : 0,
                        MPI_INT, i, 1, MPI_COMM_WORLD
                );
                left -= block;
            }

            left = N * N;
            for (i = 1; i < size; ++i) {
                MPI_Probe(i, 2, MPI_COMM_WORLD, &status);
                MPI_Get_count(&status, MPI_INT, &left);
                MPI_Recv(
                        c + (i - 1) * block, left,
                        MPI_INT, i, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE
                );
            }
            print_matrix(c, N, N, rank);

            // Transpose 'b' in 't' for matrix multiplication
            t = new int[N * N];
            transpose(b, t, N);
            // Matrix multiplication
            block = N / size;
            left = block;
            for (i = 1; i < size; ++i) {
                left_n = std::min(block, std::abs(N - left)) * N;
                if (left >= N) left_n = 0;
                MPI_Send((left >= N) ? a + left * N : a,
                         (left >= N) ? left_n : 0,
                         MPI_INT, i, 0, MPI_COMM_WORLD);
                block = N / size;
                left = block;
                for (j = 1; i < size; ++i) {
                    MPI_Send((left >= N) ? b + left * N : b,
                             (left >= N) ? left_n : 0,
                             MPI_INT, i, 1, MPI_COMM_WORLD);
                    left_n += block;
                }
            }

            for (i = 1; i < size; ++i) {
                MPI_Recv(
                        c + i * (N / size * N), N / size * N,
                        MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE
                );
            }
            print_matrix(c, N, N, rank);
        } else {
            // Simple multiplication
            MPI_Probe(0, 0, MPI_COMM_WORLD, &status);
            MPI_Get_count(&status, MPI_INT, &block);
            a = new int[block];
            b = new int[block];
            MPI_Recv(a, block, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Recv(b, block, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            for (i = 0; i < block; ++i) {
                a[i] = a[i] * b[i];
            }
            MPI_Send(a, block, MPI_INT, 0, 2, MPI_COMM_WORLD);

            /*// Matrix multiplication
            MPI_Probe(0, 3, MPI_COMM_WORLD, &status);
            MPI_Get_count(&status, MPI_INT, &block);
            a = new int[block];
            d = new int[1];
            MPI_Recv(a, block, MPI_INT, 0, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Recv(d, 1, MPI_INT, 0, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            *//*printf("%d received:\t", rank);
            print_block(a, block, rank);*//*
            t = new int[block];
            for (z = 0; z < d[0]; ++z) {
                MPI_Probe(0, 5 * (z + 1), MPI_COMM_WORLD, &status);
                MPI_Get_count(&status, MPI_INT, &left);
                b = new int[left];
                MPI_Recv(b, left, MPI_INT, 0, 5 * (z + 1), MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                *//*printf("%d received #2:\t", rank);
                print_block(b, left, rank);*//*
                for (i = 0; i < block / N; ++i) {
                    for (j = 0; j < block / N; ++j) {
                        t[N * i + j + z * block / N] = 0;
                        for (k = 0; k < N; ++k) {
                            t[N * i + j + z * block / N] += a[N * i + k] * b[N * j + k];
                        }
                    }
                }
            }
            MPI_Send(t, block, MPI_INT, 0, 6, MPI_COMM_WORLD);*/
        }
    }

    MPI_Finalize();
}

