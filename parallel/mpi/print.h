/*
 * Author: Eldar Mingachev
 */

#include <iostream>

#ifndef MPI_PRINT_H
#define MPI_PRINT_H

void print_block(int *a, const int &size, const int &rank, const int &global) {
    if (size == 0) {
        printf("Process %d (%d global) received: none.\n", rank, global);
    } else {
        printf("Process %d (%d global) received: [", rank, global);
        for (size_t i = 0; i < size; ++i) {
            printf("%d, ", *(a + i));
        }
        printf("\b\b].\n");
    }
}

void print_block(int *a, const int &size, const int &rank) {
    if (size == 0) {
        printf("Process %d received: none.\n", rank);
    } else {
        printf("Process %d received: [", rank);
        for (size_t i = 0; i < size; ++i) {
            printf("%d, ", *(a + i));
        }
        printf("\b\b].\n");
    }
}

void print_matrix(const int *a, const int &rows, const int &cols, const int &rank) {
    if (rows == 0 && cols == 0) {
        printf("Process %d received: none.\n", rank);
    } else {
        std::string result = "Process " + std::to_string(rank) + " received: [\n";
        for (size_t i = 0; i < rows; ++i) {
            result += "( ";
            for (size_t j = 0; j < cols; ++j) {
                result += std::to_string(a[i * cols + j]) + ", ";
            }
            result += "\b\b )\n";
        }
        std::cout << result + "]\n";
    }
}

#endif //MPI_PRINT_H
