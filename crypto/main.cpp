/** Author: Mingachev Eldar **/

#include <iostream>
#include <cstring>
#include "Twofish.h"

int main() {
    unsigned char key[] = {00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00};
  //  char in[] = {};
//strcpy(in, key);
    unsigned char block[16] = {0x80, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00};
    /*for (int i = 0; i < 16; ++i) {
        block[i] = in[i] + 256;
    }*/

    TwoFish twoFish(key, sizeof(key));

    FILE *o = fopen("test", "wb");
    fwrite(block, 16, 1, o);
    twoFish.encrypt(block);
    fwrite(block, 16, 1, o);
    twoFish.decrypt(block);
    fwrite(block, 16, 1, o);
    fclose(o);

    return 0;
}